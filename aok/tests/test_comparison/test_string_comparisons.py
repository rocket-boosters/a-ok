import pytest

import aok


def test_contains():
    """Should successfully compare."""
    aok.contains("foo").compare("this is a foo problem.").raise_for_assertion()


def test_contains_error():
    """Should successfully compare."""
    with pytest.raises(AssertionError):
        aok.contains("bar").compare("this is a foo problem.").raise_for_assertion()


def test_not_contains():
    """Should successfully compare."""
    aok.not_contains("bar").compare("this is a foo problem.").raise_for_assertion()


def test_not_contains_error():
    """Should successfully compare."""
    with pytest.raises(AssertionError):
        aok.not_contains("foo").compare("this is a foo problem.").raise_for_assertion()


def test_not_like():
    """Should successfully compare."""
    aok.not_like("ba*").compare("this is a foo problem.").raise_for_assertion()


def test_not_lie_error():
    """Should successfully compare."""
    with pytest.raises(AssertionError):
        aok.not_like("*fo*").compare("this is a foo problem.").raise_for_assertion()
